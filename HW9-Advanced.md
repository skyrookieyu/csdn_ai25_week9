# 1. 使用同一相机，在两个不同的位姿分别拍摄同一个棋盘格，然后使用本质矩阵估计两次拍摄间的平移和旋转(提示：可参考https://blog.csdn.net/qqh19910525/article/details/52240521)

#### 本次标定，是使用同一个手机（ASUS Zenfone 4 Pro - 上个作业已经做过校验，相关内参矩阵和畸变矩阵等参数已知），两次摆拍位置有较大旋转角度（为了棋盘格处于中心位置）。

####### 相机内参矩阵 #######

[[2.94209185e+03 0.00000000e+00 1.41864338e+03]  
 [0.00000000e+00 2.94475269e+03 1.90408772e+03]  
 [0.00000000e+00 0.00000000e+00 1.00000000e+00]]  
 
####### 畸变系数矩阵 #######

[[ 0.15451441 -0.74656771 -0.00105584 -0.00187541  0.86391367]]


```python
import cv2
import numpy as np
import matplotlib.pyplot as plt
from math import degrees as dg
from math import sqrt
import math

# 加载图片
img1 = cv2.imread('images_hw9/L1.jpg', 0)
img2 = cv2.imread('images_hw9/R1.jpg', 0)

# 初始化 SIFT 方法
sift = cv2.xfeatures2d_SIFT.create()

# 获取关键点和描述子
k1, d1 = sift.detectAndCompute(img1, None)
k2, d2 = sift.detectAndCompute(img2, None)

# 设置 FLANN 超参数
FLANN_INDEX_KDTREE = 0

# k-d 树索引超参数
index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=5)

# 搜索超参数
search_params = dict(checks=50)

# 初始化 FlannBasedMatcher 匹配器
flann = cv2.FlannBasedMatcher(index_params, search_params)

# 通过 KNN 的方式匹配两张图的描述子
matches = flann.knnMatch(d1, d2, k=2)

good = []
pts1 = []
pts2 = []

# 筛选比较好的匹配点
for i, (m, n) in enumerate(matches):
    if m.distance < 0.8 * n.distance:
        good.append(m)
        pts2.append(k2[m.trainIdx].pt)
        pts1.append(k1[m.queryIdx].pt)

# 计算基础矩阵
pts1 = np.int32(pts1)
pts2 = np.int32(pts2)

# F为基本矩阵、mask是返回基本矩阵的值：没有找到矩阵，返回 0，找到一个矩阵返回 1，多个矩阵返回 3
F, mask = cv2.findFundamentalMat(pts1, pts2) #, cv2.FM_LMEDS)

# 只选择有效数据
pts1 = pts1[mask.ravel() == 1]
pts2 = pts2[mask.ravel() == 1]


def drawlines(img1, img2, lines, pts1, pts2):
    """
    绘制图像极线
    :param img1:
    :param img2:
    :param lines:
    :param pts1:
    :param pts2:
    :return:
    """
    r, c = img1.shape
    img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)
    img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)

    for r, pt1, pt2 in zip(lines, pts1, pts2):
        color = tuple(np.random.randint(0, 255, 3).tolist())
        x0, y0 = map(int, [0, -r[2] / r[1]])
        x1, y1 = map(int, [c, -(r[2] + r[0] * c) / r[1]])

        img1 = cv2.line(img1, (x0, y0), (x1, y1), color, 1)
        img1 = cv2.circle(img1, tuple(pt1), 5, color, -1)
        img2 = cv2.circle(img2, tuple(pt2), 5, color, -1)
    return img1, img2


# 在右图（第二图）中找到与点相对应的极线，并在左图上画出它的线。
lines1 = cv2.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
lines1 = lines1.reshape(-1, 3)
img5, img6 = drawlines(img1, img2, lines1, pts1, pts2)

# 找到与左图像（第一个图像）中的点对应的极线，以及在右图上画线
lines2 = cv2.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
lines2 = lines2.reshape(-1, 3)
img3, img4 = drawlines(img2, img1, lines2, pts2, pts1)
cv2.imwrite("images_hw9/L1_Lines.jpg", img5)
cv2.imwrite("images_hw9/R1_Lines.jpg", img3)
#plt.subplot(121), plt.imshow(img5)
#plt.subplot(122), plt.imshow(img3)
#plt.show()

# 加载相机标定的内参数、外参数矩阵
with np.load('images_hw9/Camera.npz') as X:
    mtx, dist, _, _ = [X[i] for i in ('mtx', 'dist', 'rvecs', 'tvecs')]
print("内参矩阵\n", mtx, "\n")

# 镜头旋转 90 度，所以内参数矩阵略作调整
# mtx_horizontal = np.array([[2.94475269e+03, 0.00000000e+00, 1.90408772e+03],
#                            [0.00000000e+00, 2.94209185e+03, 1.41864338e+03],                     
#                            [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]], 
#                          dtype=np.float32)
#mtx_horizontal = np.array([[mtx[1, 1], 0.00000000e+00, mtx[1, 2]],
#                           [0.00000000e+00, mtx[0, 0], mtx[0, 2]],                     
#                           [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]], 
#                          dtype=np.float32)

# 使用本质矩阵估计两次拍摄间的平移和旋转
# E, masks = cv2.findEssentialMat(pts1, pts2, np.array(mtx, dtype=np.float32), cv2.FM_LMEDS)
E, masks = cv2.findEssentialMat(pts1, pts2, mtx)
print("本质矩阵\n", E, "\n")
# print("相对参数", masks)

# SVD 分解
# print("Singular Value Decomposition(SVD)\n")

# 创建矩阵A
# A = np.array(E)

# 利用 np.linalg.svd() 函数直接进行奇异值分解
# 该函数有三个返回值：左奇异矩阵，所有奇异值，右奇异矩阵。
# U, Sigma, VT = np.linalg.svd(A)

# 展示
# print("Rotation\n", U, "\n")
# print("Sigma\n", Sigma, "\n")
# print("Translation\n", VT, "\n")

# 本质矩阵分解成 R,t 矩阵
retval2, R, t, mask = cv2.recoverPose(E, pts1, pts2, mtx)

#print("retval2:", retval2)
print("R 矩阵\n", R, "\n")
print("t 矩阵\n", t, "\n")

#dstR, _ = cv2.Rodrigues(R)

#sita = []
#for i in range(3):
#    sita = dg(R[0][i])
#    print("sita %d is %f"%(i, sita))
    
# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = np.float32)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
 
# Calculates rotation matrix to euler angles
# The result is the same as MATLAB except the order
# of the euler angles ( x and z are swapped ).
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])

angles = rotationMatrixToEulerAngles(R) 
# print(angles[0], angles[1], angles[2])
print("alpha = ", dg(angles[0]), " beta = ", dg(angles[1]), " gamma = ", dg(angles[2]))

value = sqrt(t[0][0] ** 2 + t[1][0] ** 2 + t[2][0] ** 2)
print("T value is ", value)
```

    内参矩阵
     [[2.94209185e+03 0.00000000e+00 1.41864338e+03]
     [0.00000000e+00 2.94475269e+03 1.90408772e+03]
     [0.00000000e+00 0.00000000e+00 1.00000000e+00]] 
    
    本质矩阵
     [[-0.21838996 -0.61824809  0.06083519]
     [ 0.62661205 -0.22342383 -0.23431817]
     [ 0.03765065  0.25972271 -0.00590562]] 
    
    R 矩阵
     [[ 0.94340024 -0.32978166  0.0352142 ]
     [ 0.3304774   0.94367756 -0.01604199]
     [-0.02794049  0.02677152  0.99925103]] 
    
    t 矩阵
     [[-0.36434656]
     [-0.07119167]
     [-0.92853828]] 
    
    alpha =  1.5346775578634093  beta =  1.6010807651083498  gamma =  19.30558997136349
    T value is  0.9999999999999999
    

![L1](images_hw9/L1_Lines.jpg) ![R1](images_hw9/R1_Lines.jpg)


```python
import cv2
import numpy as np
import glob
from tqdm import tqdm
from math import degrees as dg
from math import sqrt

chessboard_size = (5, 7)

# 使用畸变矩阵去除图片畸变，并且适度裁剪
def get_optimal_pic(path, pic_name, mtx, dist):

    img2 = cv2.imread(path + str(pic_name))
    # print("orgininal img_point  array shape",img.shape)

    h, w = img2.shape[:2]
    # print("pic's hight, weight: %f,  %f"%(h, w))

    new_mtx, roi = cv2.getOptimalNewCameraMatrix(
        mtx, dist, (w, h), 1, (w, h))  # 自由比例参数

    new_pic = cv2.undistort(img2, mtx, dist, None, new_mtx)

    # 根据前面ROI区域裁剪图片
    x, y, w, h = roi
    new_pic = new_pic[y:y + h, x:x + w]
    pic_new_name = str("opt_") + str(pic_name)
    new_path_name = str(path) + pic_new_name
    cv2.imwrite(new_path_name, new_pic)
    #print("##################" + str(pic_new_name) + "##################")
    #print("get new optimal ROI picture")
    return pic_new_name

# 寻找图片角点，并且标注角点位置
def get_corners_mat(path, pic_name, optal_or_not):
    img = cv2.imread(str(path) + str(pic_name))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    ret, corners = cv2.findChessboardCorners(gray, chessboard_size, None)
    if ret == True:
        corners2 = cv2.cornerSubPix(gray, corners, (5, 5), (-1, -1), criteria)
        img = cv2.drawChessboardCorners(img, chessboard_size, corners2, ret)
              
        count = 0

        for corner in corners:
            x_label,y_label = corner[0]
            # print("corner_num:"+str(count)+" /location:"+str(corner))
            cv2.putText(img, '(%s)' % (count), (x_label,y_label), cv2.FONT_HERSHEY_COMPLEX, 0.5, (255, 255, 255), 1)
            count = count + 1
        if optal_or_not :
            cv2.imwrite(path + "corner_" + str(pic_name), img) 
        else:
            cv2.imwrite(path + "orig_" + "corner_" + str(pic_name), img)
        cv2.destroyAllWindows()
        # print("Chessboard found!")
    else:
        print("Failed to find chessboard!")
        return None
    
    return corners

# 图片去畸变和适度裁剪
left_pic_name = get_optimal_pic("images_hw9/","L1.jpg", mtx, dist)
right_pic_name = get_optimal_pic("images_hw9/","R1.jpg", mtx, dist)

# 读取原始图片左右相机图片
# org_corners_rt = get_corners_mat("./picture_used/","left.jpg", False)
# org_corners_lf = get_corners_mat("./picture_used/","right.jpg", False)

# 读取去畸变之后、左右相机图片
corners_rt = get_corners_mat("images_for_calibration_hw9/", right_pic_name, True)
corners_lf = get_corners_mat("images_for_calibration_hw9/", left_pic_name, True)

# 带入参数寻找本质矩阵（使用原始图片角点）
# E, mask = cv2.findEssentialMat(org_corners_lf, org_corners_rt,
#                                cameraMatrix=mtx, method=cv2.RANSAC,
#                                threshold=1, prob=0.999
#                                )
# print('E: ', E)

# 带入参数寻找本质矩阵（使用去畸变之后的角点）
E, mask = cv2.findEssentialMat(corners_lf, corners_rt,
                               cameraMatrix=mtx, method=cv2.RANSAC,
                               threshold=1, prob=0.999
                               )
print("本质矩阵\n", E, "\n")

# 本质矩阵分解成 R,t 矩阵
# retval2, R, t, mask = cv2.recoverPose(E, org_corners_lf, org_corners_rt, mtx)
retval2, R, t, mask = cv2.recoverPose(E, corners_lf, corners_rt, mtx)

# print("retval2:", retval2)
# print("##"*10)
print("R 矩阵\n", R, "\n")
print("t 矩阵\n", t, "\n")

#dstR, _ = cv2.Rodrigues(R)

#sita = []
#for i in range(3):
#    sita = dg(R[0][i])
#    print("sita %d is %f"%(i, sita))

# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = np.float32)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
 
# Calculates rotation matrix to euler angles
# The result is the same as MATLAB except the order
# of the euler angles ( x and z are swapped ).
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])

angles = rotationMatrixToEulerAngles(R) 
# print(angles[0], angles[1], angles[2])
print("alpha = ", dg(angles[0]), " beta = ", dg(angles[1]), " gamma = ", dg(angles[2]))

value = sqrt(t[0][0] ** 2 + t[1][0] ** 2 + t[2][0] ** 2)
print("T value is ", value)
```

    本质矩阵
     [[-0.23312276 -0.65454314  0.08606462]
     [ 0.6559511  -0.23354915  0.09327592]
     [-0.10915413 -0.06622486  0.00146456]] 
    
    R 矩阵
     [[ 0.94165373 -0.33647939 -0.00835833]
     [ 0.33641753  0.94167905 -0.00798821]
     [ 0.01055873  0.00471024  0.99993316]] 
    
    t 矩阵
     [[ 0.14014237]
     [-0.11386446]
     [-0.9835624 ]] 
    
    alpha =  0.2698927044973505  beta =  -0.6049818605964925  gamma =  19.659901590947566
    T value is  0.9999999999999998
    


```python
import numpy as np
import cv2
import glob

# termination criteria
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
chessboard_size = (5, 7)

# 世界坐标系下的 3D 坐标， 棋盘格大小是（36mm * 36mm）
objp = np.zeros((np.prod(chessboard_size), 3), dtype=np.float32)
objp[:, :2] = np.mgrid[0:chessboard_size[0],
                       0:chessboard_size[1]].T.reshape(-1, 2)*36

# Arrays to store object points and image points from all the images.
objpointsL = []  # 3d point in real world space
imgpointsL = []  # 2d points in image plane.
objpointsR = []
imgpointsR = []

images = glob.glob('images_hw9/L1.jpg')
for fname in images:
    img = cv2.imread(fname)
    grayL = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, cornersL = cv2.findChessboardCorners(grayL, chessboard_size, None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        objpointsL.append(objp)
        # print("L True\n")
        cornersL2 = cv2.cornerSubPix(grayL, cornersL, (11, 11), (-1, -1), criteria)
        imgpointsL.append(cornersL2)

images = glob.glob('images_hw9/R1.jpg')
for fname in images:
    img = cv2.imread(fname)
    grayR = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Find the chess board corners
    ret, cornersR = cv2.findChessboardCorners(grayR, chessboard_size, None)

    # If found, add object points, image points (after refining them)
    if ret == True:
        # print("R True\n")
        objpointsR.append(objp)

        cornersR2 = cv2.cornerSubPix(grayR, cornersR, (11, 11), (-1, -1), criteria)
        imgpointsR.append(cornersR2)

retval, cameraMatrix1, distCoeffs1, cameraMatrix2, distCoeffs2, R1, T1, E1, F1 = cv2.stereoCalibrate(
    objpointsL, imgpointsL, imgpointsR, mtx, dist, mtx, dist, (2936, 3916))

print("本质矩阵\n", E1, "\n")
print("R 矩阵\n", R1, "\n")
print("t 矩阵\n", T1, "\n")

#dstR1, _ = cv2.Rodrigues(R1)

#sita1 = []
#for i in range(3):
#    sita1 = dg(R1[0][i])
#    print("sita %d is %f"%(i, sita1))
    
# Checks if a matrix is a valid rotation matrix.
def isRotationMatrix(R) :
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype = np.float32)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6
 
# Calculates rotation matrix to euler angles
# The result is the same as MATLAB except the order
# of the euler angles ( x and z are swapped ).
def rotationMatrixToEulerAngles(R) :
 
    assert(isRotationMatrix(R))
     
    sy = math.sqrt(R[0,0] * R[0,0] +  R[1,0] * R[1,0])
     
    singular = sy < 1e-6
 
    if  not singular :
        x = math.atan2(R[2,1] , R[2,2])
        y = math.atan2(-R[2,0], sy)
        z = math.atan2(R[1,0], R[0,0])
    else :
        x = math.atan2(-R[1,2], R[1,1])
        y = math.atan2(-R[2,0], sy)
        z = 0
 
    return np.array([x, y, z])

angles = rotationMatrixToEulerAngles(R) 
# print(angles[0], angles[1], angles[2])
print("alpha = ", dg(angles[0]), " beta = ", dg(angles[1]), " gamma = ", dg(angles[2]))

#value = sqrt(T1[0][0] ** 2 + T1[1][0] ** 2 + T1[2][0] ** 2)
#print("T value is ", value)
```

    本质矩阵
     [[  8.71012141  24.41869418  -3.98621784]
     [-24.81936966   8.82520605   9.69994345]
     [ -0.04596913 -11.27556335   0.25685344]] 
    
    R 矩阵
     [[ 0.94311828 -0.33044102  0.03656013]
     [ 0.33103527  0.94354908 -0.01143597]
     [-0.03071736  0.02288817  0.99926602]] 
    
    t 矩阵
     [[-10.65720221]
     [ -3.69194486]
     [-25.96918012]] 
    
    alpha =  0.2698927044973505  beta =  -0.6049818605964925  gamma =  19.659901590947566
    

#### 本质矩阵，相当于基础矩阵的标准化（相关内参为1）；直观理解，其本身只有 5 个自由度，沿着 X, Y, Z 轴相关方向旋转，再沿着 X, Y, Z 轴平移。利用 5 点法最后分解出来的相关位移是单位向量，即还需要乘以一个比例系数（这个比例系数未知）才是实际位移。

#### 所以以上过程只是求得了两相机的相对位姿，即 R, t。

#### 这三种算法的结果差距颇大，本质矩阵及 t 向量差异都很大，只有 R 矩阵的对角线上元素的值很接近。

#### 从 R 矩阵的计算结果可以知道，进行摆拍时几乎只在 X-Y 平面上旋转，也就是只绕着 Z 轴旋转而已，此推论符合实际拍摄的情况，约绕 Z 轴旋转 20 度。


# 2. 使用一本书，在两个不同位姿拍摄，然后使用任一种匹配方法计算特征点，完成匹配，并标出匹配结果。


```python
import cv2
import glob
import numpy as np
import math
from matplotlib import pyplot as plt
import matplotlib
# %matplotlib inline

img_left = cv2.imread('images_hw9/BookL0.jpg')
img_right = cv2.imread('images_hw9/BookR0.jpg')

# SIFT
def SIFT_Matcher(img1, img2):
    gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    maxcorners = 1000

    sift = cv2.xfeatures2d_SIFT.create(nfeatures=maxcorners)
    kp1, descriptors1 = sift.detectAndCompute(gray1, None)
    kp2, descriptors2 = sift.detectAndCompute(gray2, None)

    bfMatcher = cv2.FlannBasedMatcher()
    matches = bfMatcher.knnMatch(descriptors1, descriptors2, k=2)

    good = [[m] for m, n in matches if m.distance < 0.4 * n.distance]
    img3 = cv2.drawMatchesKnn(img1, kp1, img2, kp2, good, None, flags=2)

    cv2.imwrite("images_hw9/BookSIFT.jpg", img3)
    cv2.destroyAllWindows()
    print("SIFT Completed!\n")

# SURF
def SURF_Matcher(img1, img2):
    gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    minThreashold = 1000  # hession 矩阵阈值，在这里调整精度，值越大点越少，越精准
    surf = cv2.xfeatures2d_SURF.create(minThreashold)
    keypoints, descriptor = surf.detectAndCompute(gray1, None)
    keypoints2, descriptor2 = surf.detectAndCompute(gray2, None)

    bfMatcher = cv2.FlannBasedMatcher()
    matches = bfMatcher.knnMatch(descriptor, descriptor2, k=2)

    good = [[m] for m, n in matches if m.distance < 0.5 * n.distance]
    img3 = cv2.drawMatchesKnn(img1, keypoints, img2, keypoints2, good, None, flags=2)

    cv2.imwrite("images_hw9/BookSURF.jpg", img3)
    cv2.destroyAllWindows()
    print("SURF Completed!\n")

# ORB
def ORB_Matcher(img1, img2):
    gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    nkeypoint = 100  # 算法在图片中找到匹配点的对数

    orb = cv2.ORB.create(nkeypoint)

    kp1, descriptors1 = orb.detectAndCompute(gray1, None)
    kp2, descriptors2 = orb.detectAndCompute(gray2, None)

    bf = cv2.DescriptorMatcher.create('BruteForce')
    matches = bf.match(descriptors1, descriptors2)

    img3 = cv2.drawMatches(img1, kp1, img2, kp2, matches, None, flags=2)

    cv2.imwrite("images_hw9/BookORB.jpg", img3)
    cv2.destroyAllWindows()
    print("ORG Completed!\n")
    
    
SIFT_Matcher(img_left, img_right)
SURF_Matcher(img_left, img_right)
ORB_Matcher(img_left, img_right)

```

    SIFT Completed!
    
    SURF Completed!
    
    ORG Completed!
    
    

SIFT：  
  
![SIFT](images_hw9/BookSIFT.jpg)  
  
SURF：  
  
![SURF](images_hw9/BookSURF.jpg)  
  
ORB:  
    
![ORB](images_hw9/BookORB.jpg)  
  

#### 以拍的这本书(侍茶师)来看，SURF的效果最佳，虽然也有不少错误。 SIFT正确率不低，但遗失很多匹配，ORB虽然对于灰度明显高度落差的特征点撷取效果最好(领带上的黑钮扣)，但匹配的正确率却不高。


```python

```
